import { Box, Typography, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';
import { InlineInput } from './inlineInput';

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { name, email },
  sx,
}) => {
  const onFieldSave = async (field: string, value: string) => {
    console.log(`Sending ${field} change to server...`, value);
    await new Promise((r) => setTimeout(r, 250));
  };

  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box textAlign="center" mt={2}>
          <InlineInput text={name} name="name" onFieldSave={onFieldSave} />
          <InlineInput text={email} name="email" onFieldSave={onFieldSave} />
        </Box>
      </Box>
    </Card>
  );
};
