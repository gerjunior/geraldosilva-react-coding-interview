import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import { SystemStyleObject, Theme } from '@mui/system';
import { useState } from 'react';

type IInlineInputProps = {
  name: string;
  text: string;
  onFieldSave: (field: string, value: string) => void;
  sx?: SystemStyleObject<Theme>;
};

export const InlineInput: React.FC<IInlineInputProps> = ({
  sx,
  text,
  name,
  onFieldSave,
}) => {
  const [isEditing, setIsEditing] = useState(false);
  const [editValue, setEditValue] = useState(text);

  const handleClick = () => {
    setIsEditing(true);
  };

  return (
    <>
      {!isEditing && (
        <Typography
          sx={sx}
          variant="subtitle1"
          lineHeight="1rem"
          onClick={handleClick}
        >
          {editValue}
        </Typography>
      )}

      {isEditing && (
        <Box>
          <TextField
            value={editValue}
            onChange={(e) => setEditValue(e.target.value)}
          />
          <Button onClick={() => onFieldSave(name, editValue)}>Save</Button>
          <Button>Cancel</Button>
        </Box>
      )}
    </>
  );
};
